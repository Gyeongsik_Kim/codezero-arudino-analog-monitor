﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Drawing.Printing;
using System.Windows.Forms.DataVisualization.Charting;
namespace monitor {using System.Drawing.Printing;
    public partial class Form1 : Form {
        
        private PageSettings pgSettings = new PageSettings();
        private PrinterSettings prtSettings = new PrinterSettings();
        private PrintDocument printDocument1 = new PrintDocument();
        private short[] value;
        private const byte CR = 0x0d;
        private const byte LF = 0x0a;
        private System.IO.Ports.SerialPort serial;
        private Series[] series;
        private Thread thread;
        private static bool isClose;
        private Bitmap memoryImage;
        private delegate void EnableChart (Series series, bool state);
        private delegate void AddChartData (Series series, double X, double Y);

        public Form1 () {
            InitializeComponent();
            isClose=false;
            value=new short[8];
            series=new Series[8];
            graphInit();
            dateTimePicker1.MinDate=DateTime.Now;
            dateTimePicker2.MinDate=DateTime.Now;
            this.checkedListBox1.Items.Add("이전 파일에 이어서 저장", false);
            CheckForIllegalCrossThreadCalls=false;
            printDocument1.PrintPage+=new PrintPageEventHandler(printDocument1_PrintPage);

        }

        private void button1_Click (object sender, EventArgs e) {
            try{
                System.ComponentModel.IContainer components = new System.ComponentModel.Container();
                serial=new System.IO.Ports.SerialPort(components);
                serial.PortName="COM"+numCom.Value.ToString();
                serial.DataBits=8;
                serial.BaudRate=9600;
                serial.Parity=System.IO.Ports.Parity.None;
                serial.StopBits=System.IO.Ports.StopBits.One;
                serial.Open();
                
                if(dateTimePicker1.Value>dateTimePicker2.Value)
                    MessageBox.Show("시간을 확인해주세요");

                Debug.WriteLine(dateTimePicker1.Text);
                Debug.WriteLine(dateTimePicker2.Text);
                button1.Text="연결 성공!";
                button1.Enabled=false;
                thread = new Thread(new ThreadStart(Thread_Monitoring));
                thread.Start();
            }catch(Exception) {
                button1.Text="실패...";
                button1.Enabled=true;
                serial.Close();
                serial.Dispose();
            }

        }
        string getDirectory (string path) {
            if(System.IO.Directory.Exists(path)) {
                string result = "";
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
                foreach(var item in di.GetFiles()) {
                    if(item.Name.Contains(".csv")) {
                        result=item.Name;
                    }
                }
                return result;
            }
            return null;
        }
        private void Form1_FormClosing (Object sender, FormClosingEventArgs e) {
            isClose=true;
        }

        public void Thread_Monitoring () {
        FileStream file;

        Regex str = new Regex(",");
            Debug.WriteLine("Start");
            Debug.WriteLine(dateTimePicker1.Value.ToOADate());
            Debug.WriteLine(dateTimePicker2.Value.ToOADate());
            long start = dateTimePicker1.Value.ToFileTime();
            long end = dateTimePicker2.Value.ToFileTime();
            string fileName ="";
            if(checkedListBox1.GetItemCheckState(8) == CheckState.Checked) {
                Debug.Write("add check");
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                path=System.IO.Path.GetDirectoryName(path);
                fileName=getDirectory(path);
                file=System.IO.File.Open(fileName, FileMode.Append);
            } else {
                Debug.Write("make check");
                string firstLine = "Time,채널0,채널1,채널2,채널3,채널4,채널5,채널6,채널7";
                fileName=DateTime.Now.ToString("yyyyMMdd hhmmss")+".csv";
                file=System.IO.File.Open(fileName, FileMode.OpenOrCreate);
                file.Write(StringToByte(firstLine), 0, firstLine.Length);
                file.WriteByte(CR);
                file.WriteByte(LF);
            }
            Debug.WriteLine("File Ready");

            this.chart1.ChartAreas[0].AxisX.Minimum=dateTimePicker1.Value.ToOADate();
            this.chart1.ChartAreas[0].AxisX.Maximum=dateTimePicker2.Value.ToOADate();

            //시작 시간 전까지 무한 루프
            while(DateTime.Now.ToFileTime()<start) ;
            Debug.WriteLine("Start Ready");

            string valueData = "";

            //끝나는 시간 전까지 루프
            Debug.WriteLine("Start");
            Debug.WriteLine((DateTime.Now.ToFileTime()<end)&&!isClose);
            while((DateTime.Now.ToFileTime()<end)&&!isClose) {
                Debug.WriteLine("Chart1 Setting Done");
                char data = (Char)serial.ReadChar();
                if(data==':') {
                    valueData=valueData+data;
                    if(int.Parse(str.Matches(valueData, 0).Count.ToString())!=7) {
                        Debug.WriteLine(str.Matches(valueData, 0).Count.ToString());
                        valueData="";
                        continue;
                    }
                    if(valueData.LastIndexOf(':')!=valueData.IndexOf(':'))
                        valueData=valueData.Substring(valueData.IndexOf(":")+1);
                    Debug.WriteLine(valueData);
                    string temp = DateTime.Now.ToString("yyyyMMdd hhmmss,");
                    for(int i = 0; i<8; i++) {
                        if(i < 7) {
                            value[i]=Convert.ToInt16(valueData.Substring(0, valueData.IndexOf(',')));
                            valueData=valueData.Substring(valueData.IndexOf(',')+1);
                        } else {
                            value[i]=Convert.ToInt16(valueData.Substring(0, valueData.IndexOf(':')));
                        }
                        if(checkedListBox1.GetItemCheckState(i)==CheckState.Checked && i != 7) {
                            temp=temp+value[i]+",";
                        }else if(checkedListBox1.GetItemCheckState(i)==CheckState.Checked && i== 7) {
                            temp=temp+value[i];
                        }else {
                            temp=temp+",";
                        }
                        Debug.WriteLine("A"+i+" Value : "+Convert.ToString(value[i]));
                        /* X축 현재시간, Y축 값 설정 */
                        if(chart1.InvokeRequired) {
                            AddChartData addChartData = new AddChartData(addChart);
                         this.Invoke(addChartData, new object[] {series[i], DateTime.Now.ToOADate(), Convert.ToDouble(value[i]) });
                        }else {
                            addChart(series[i], DateTime.Now.ToOADate(), Convert.ToDouble(value[i]));
                        }
                       series[i].Points.AddXY(DateTime.Now.ToOADate(), Convert.ToDouble(value[i]));
                    }

                    Debug.WriteLine(temp);
                    file.Write(StringToByte(temp), 0, temp.Length);
                    file.WriteByte(CR);
                    file.WriteByte(LF);
                    valueData="";
                    Thread.Sleep(1000);
                } else {
                    valueData=valueData+data;
                    Debug.WriteLine(valueData);
                }
            }
            Debug.WriteLine("Done");
            button1.Enabled=true;
            serial.Close();
        }
        
        private static byte[] StringToByte(String str) {
            return Encoding.UTF8.GetBytes(str);
        }
        private static void fileWriteLine(FileStream file, String data) {
            file.Write(StringToByte(data), 0, data.Length);
            file.WriteByte(CR);
            file.WriteByte(LF);
        }
        private void addChart (Series series, double now, double y) {
            series.Points.AddXY(now, y);
        }
        private void enableChart(Series series, bool state) {
            series.Enabled=state;
        }
        private void graphInit () {
            this.chart1.Series.Clear(); //default series를 삭제한다.
            for(int i = 0; i<8; i++) {
                this.checkedListBox1.Items.Add("채널"+i, true);
                series[i] = this.chart1.Series.Add("채널"+Convert.ToString(i));
                series[i].ChartType=SeriesChartType.Line;
                series[i].XValueType=ChartValueType.DateTime;
                
            }
            this.chart1.ChartAreas[0].AxisY.Maximum=1024;
            this.chart1.ChartAreas[0].AxisY.Minimum=0;
        }

        private Bitmap ScreenCapture() {
            Point point = new Point(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);
            Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Debug.WriteLine(bitmap.Size.ToString());
            Debug.WriteLine(point.ToString());
            Graphics g = Graphics.FromImage(bitmap);
            g.CopyFromScreen(new Point(0,0), new Point(0, 0), bitmap.Size);
            bitmap.Save("test.bmp");
            return bitmap;

        }

        private void button2_Click (object sender, EventArgs e) {
            memoryImage=ScreenCapture();
            printDocument1.PrinterSettings=prtSettings;
            printDocument1.DefaultPageSettings=pgSettings;
            PrintDialog dlg = new PrintDialog();
            dlg.Document=printDocument1;
            if(dlg.ShowDialog()==DialogResult.OK) { 
                printDocument1.Print();
            }
    }
        private void printDocument1_PrintPage (System.Object sender,
            System.Drawing.Printing.PrintPageEventArgs e) {
            e.Graphics.DrawImage(memoryImage, 50, 100);
        }

        private void Form1_Load (object sender, EventArgs e) {
                this.WindowState=FormWindowState.Maximized;
        }
    }
}
